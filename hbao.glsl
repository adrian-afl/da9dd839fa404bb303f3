vec3 hbao(){
    vec3 posc = texture(worldPosTex, UV).rgb;
    vec3 norm = texture(normalsTex, UV).rgb;
    float buf = 0, counter = 0, div = 1.0/(length(posc)+1.0);
    for(float g = 0; g < mPI2; g+=0.52){
        for(float g2 = 0.1; g2 < 1.0; g2+=0.29){
            vec3 pos = texture(worldPosTex,  UV + (vec2(sin(g + g2)*ratio, cos(g + g2)) * (getRand() * 4.09)) * div).rgb;
            buf += max(0, sign(length(posc) - length(pos))) * (dot(norm, normalize(pos - posc))) * max(0, (10.0 - length(pos - posc))/10.0);
            counter+=0.3;
        }
    }
    return 0.2*vec3(0.1, 0.13, 0.19) * (1.0 - min(buf / counter, 0.9));
}